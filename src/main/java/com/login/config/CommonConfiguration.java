package com.login.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories (basePackages = "com.login.repository")
@EntityScan (basePackages = "com.login.entity")
public class CommonConfiguration  {

}
