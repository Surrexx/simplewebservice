package com.login.controller;

import com.login.entity.Authority;
import com.login.entity.User;
import com.login.repository.AuthorityRepository;
import com.login.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthorityRepository authorityRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String sayHello(){
        Authority authority = authorityRepository.findOne(1L);
        String role = authority.getAuthority();
        User user = userRepository.findOne(1L);
        String name = user.getLogin();
        return "Hello, "+name+" you are "+role+"!";
    }
}
